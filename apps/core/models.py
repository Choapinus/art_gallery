from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User

# Create your models here.

class Tag(models.Model):
	name = models.CharField(max_length=25, verbose_name='Nombre')
	created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
	updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')

	class Meta:
		verbose_name = "etiqueta"
		verbose_name_plural = "etiquetas"
		ordering = ['-created']

	def __str__(self):
		return self.name

class Illustration(models.Model):
	title = models.CharField(max_length=150, verbose_name='Titulo')
	subtitle = models.CharField(max_length=150, verbose_name='Subtitulo')
	content = models.TextField(verbose_name='Contenido')
	image = models.ImageField(verbose_name='Imagen', upload_to='illustrations')
	client = models.CharField(max_length=100, verbose_name='Cliente')
	published = models.DateTimeField(verbose_name='Fecha de publicación', default=now)
	services = models.ManyToManyField('Tag')
	designer = models.ForeignKey(User, verbose_name='Diseñador', on_delete=models.CASCADE)
	created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
	updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')


	class Meta:
		verbose_name = "ilustracion"
		verbose_name_plural = "ilustraciones"
		ordering = ['-created']

	def __str__(self):
		return self.title