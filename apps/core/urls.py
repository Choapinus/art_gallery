from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('gallery/', views.gallery, name='gallery'),
	path('slider/', views.slider, name='slider'),
	path('gallery/detail/<int:pk>', views.gallery_single, name='gallery_single'), # show by id
]
