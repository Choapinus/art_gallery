from django.contrib import admin
from .models import Illustration, Tag

# Register your models here.

class IllustrationAdmin(admin.ModelAdmin):
	readonly_fields = ('created', 'updated', )
	list_display = ('title', 'designer', 'published', )
	ordering = ('designer', 'published', )
	search_fields = ('designer__username', )

	def illustration_services(self, obj):
		return ', '.join([c for c in obj.services.all().order_by("name")])

	illustration_services.short_description = "Services"

class TagAdmin(admin.ModelAdmin):
	readonly_fields = ('created', 'updated', )

admin.site.register(Illustration, IllustrationAdmin)
admin.site.register(Tag, TagAdmin)