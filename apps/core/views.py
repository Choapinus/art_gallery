from django.shortcuts import render, HttpResponse
from .models import Illustration

# Create your views here.

def index(request):
	template_name = 'core/index.htm'
	illustrations = Illustration.objects.all()[:5]
	context = {'illustrations': illustrations}
	return render(request, template_name, context)

def gallery(request):
	template_name = 'core/index-gallery.htm'
	return render(request, template_name)

def slider(request):
	template_name = 'core/index-slider.htm'
	return render(request, template_name)

def gallery_single(request, pk):
	template_name = 'core/gallery-single.htm'
	illustration = Illustration.objects.get(id=pk)
	context = {'illustration': illustration}
	return render(request, template_name, context)